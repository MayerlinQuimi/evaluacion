/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banco;

/**
 *
 * @author FACCI
 */
public class Cuenta {
    private double saldo;
    public double Saldo;
    //si es cuenta de ahorro, corriente,plazo fijo o prestamo
    private String tipoCuenta;
    public String TipoCuenta;
    //definir el interes dependiendo el tipo de cuenta
    private double interes;
    public double Interes;
    
    private String numeroCuenta;
    public String NumeroCuenta;
    
    

    public double getSaldo() {
        return saldo;
    }
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    

    public String getTipoCuenta() {
        return tipoCuenta;
    }
    

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }
    
    public double getInteres() {
        return interes;
    }
    

    public void setInteres(double Interes) {
        this.Interes = Interes;
    }


    public String getNumeroCuenta() {
        return numeroCuenta;
    }
    

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    
    
}
